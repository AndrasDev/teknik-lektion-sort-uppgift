//including needed headers
#include <iostream>
#include <vector>

//including needed custon headers
#include "Sort.h"
#include "XORShift.h"
#include "Time.h"

//defining the size of array and elements
#define ARRAY_SIZE 500
#define ELEMENT_SIZE 1000

//defining information needed to generate numbers
#define MAX_RAND_NUM 1000
#define RAND_SEED 1323

//add usings so we dont have to type std:: infront of alot of things
//but still not using "using namespace std" so it does not take so much memory(not a big deal)
using std::endl;
using std::cin;
using std::cout;
using std::vector;

//declaring a log function so it can be used in the main function
void Log(char* message);

int main()
{
	//initializing a random number generator class
	XORShift rand = XORShift(RAND_SEED);

	//declare all vectors that we are going to sort(exept initalvector, that's just so every other list is the same)
	vector<vector<int>> initalVector = vector<vector<int>>();
	vector<vector<int>> bubble = vector<vector<int>>();
	vector<vector<int>> insertion = vector<vector<int>>();
	vector<vector<int>> cycle = vector<vector<int>>();
	vector<vector<int>> merge = vector<vector<int>>();

	//every time vector for storing the time of one sort loop
	vector<long> bubbleTime = vector<long>();
	vector<long> insertionTime = vector<long>();
	vector<long> cycleTime = vector<long>();
	vector<long> mergeTime = vector<long>();

	//delcating the vectors used to get the numbers of operations for every sort loop
	vector<int> numBubbleOperations = vector<int>();
	vector<int> numInsertionOperations = vector<int>();
	vector<int> numCycleOperations = vector<int>();
	vector<int> numMergeOperations = vector<int>();

	//the total number of opetations on each algorithm
	long totalBubbleOperations = 0;
	long totalInsertionOperations = 0;
	long totalCycleOperations = 0;
	long totalMergeOperations = 0;
	
	//total time for each algorithm
	long totalInsertionTime = 0;
	long totalCycleTime = 0;
	long totalBubbleTime = 0;
	long totalMergeTime = 0;


	Log("[Array Generating]: Start - "); Time::MilisecondsTOHMS(clock());
	
	//generating everything needed
	for (int x = 0; x <= ARRAY_SIZE; x++)
	{
		//adding a element to every vector so it wont be "out of array" later
		initalVector.push_back(vector<int>());
		cycleTime.push_back(0);
		insertionTime.push_back(0);
		bubbleTime.push_back(0);
		mergeTime.push_back(0);

		numBubbleOperations.push_back(0);
		numInsertionOperations.push_back(0);
		numCycleOperations.push_back(0);
		numMergeOperations.push_back(0);

		//generating the numbers for each element and then adding it to the vector
		for (int y = 0; y <= ELEMENT_SIZE; y++)
		{
			initalVector[x].push_back(rand.nextInt(MAX_RAND_NUM));
		}
	}

	//copying every vector in the the algorithmspecific vectors
	Log("[Array Generating]: Done - "); Time::MilisecondsTOHMS(clock());
	Log("\n");
	Log("[Copying Arrays]: Start - "); Time::MilisecondsTOHMS(clock());
	bubble = initalVector;
	insertion = initalVector;
	cycle = initalVector;
	merge = initalVector;
	Log("[Copying Arrays]: Done - "); Time::MilisecondsTOHMS(clock());
	Log("\n");

	for (int i = 0; i < initalVector.size(); i++)
	{
		cout << i << endl;

		numBubbleOperations[i] = 0;
		numCycleOperations[i] = 0;
		numInsertionOperations[i] = 0;

		//sorting with cyclesort and calculating the time it takes to do so
		Log("[Insertion Sort]: Start - "); Time::MilisecondsTOHMS(clock());
		insertionTime[i] = clock();
		Sort::insertionSort(insertion[i], numInsertionOperations[i]);
		insertionTime[i] = clock() - insertionTime[i];
		Log("[Insertion Sort]: Done - "); Time::MilisecondsTOHMS(clock());
		
		
		Log("\n");

		//sorting with cyclesort and calculating the time it takes to do so
		Log("[Cycle Sort]: Start - "); Time::MilisecondsTOHMS(clock());
		cycleTime[i] = clock();
		Sort::cycleSort(cycle[i], numCycleOperations[i]);
		cycleTime[i] = clock() - cycleTime[i];
		Log("[Cycle Sort]: Done - "); Time::MilisecondsTOHMS(clock());
		

		Log("\n");

		//sorting with cyclesort and calculating the time it takes to do so
		Log("[Bubble Sort]: Start - "); Time::MilisecondsTOHMS(clock());
		bubbleTime[i] = clock();
		Sort::bubbleSort(bubble[i], numBubbleOperations[i]);
		bubbleTime[i] = clock() - bubbleTime[i];
		Log("[Bubble Sort]: Done - "); Time::MilisecondsTOHMS(clock());

		Log("\n");

		//sorting with mergesort and calculating the time it takes to do so
		Log("[Merge Sort]: Start - "); Time::MilisecondsTOHMS(clock());
		mergeTime[i] = clock();
		Sort::mergeSort(merge[i], numMergeOperations[i]);
		mergeTime[i] = clock() - mergeTime[i];
		Log("[Merge Sort]: Done - "); Time::MilisecondsTOHMS(clock());


		//adding the time it took to every totaltime variable
		totalInsertionTime += insertionTime[i];
		totalBubbleTime += bubbleTime[i];
		totalCycleTime += cycleTime[i];
		totalMergeTime += mergeTime[i];

		//adding the amount of operations it took to every totaloperations variable
		totalInsertionOperations += numInsertionOperations[i];
		totalBubbleOperations += numBubbleOperations[i];
		totalCycleOperations += numCycleOperations[i];
		totalMergeOperations += numMergeOperations[i];
	}

	//printing out the results
	Log("\n");
	Log("[Results]\n");
	cout << "[Array Size]: " << ARRAY_SIZE << " vectors with " << ELEMENT_SIZE << " elements each" << endl;
	cout << "[Total Time]: " << clock() << "ms "; Time::MilisecondsTOHMS(clock());
	cout << "[Insertion]:	[NUM OPERATIONS]:	" <<	totalInsertionOperations << "	[Time]: " << totalInsertionTime << "ms "; Time::MilisecondsTOHMS(totalInsertionTime);
	cout << "[Cycle]:	[NUM OPERATIONS]:	" << totalCycleOperations <<"		[Time]: " << totalCycleTime << "ms "; Time::MilisecondsTOHMS(totalCycleTime);
	cout << "[Bubble]:	[NUM OPERATIONS]:	" << totalBubbleOperations << " 	[Time]: " << totalBubbleTime << "ms "; Time::MilisecondsTOHMS(totalBubbleTime);
	cout << "[Merges]:	[NUM OPERATIONS]:	"  << totalMergeOperations << " 	[Time]: " << totalMergeTime << "ms "; Time::MilisecondsTOHMS(totalMergeTime);

	cin.get();
	return EXIT_SUCCESS;
}

//defining the log function so it does what it should and so we dont get a LINKER error
void Log(char* message)
{
	cout << message;
}