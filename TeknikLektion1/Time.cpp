#include "Time.h"

std::string Time::MilisecondsTOHMS(int milliseconds)
{
	int ms = (int)milliseconds % 1000;
	int seconds = (int)(milliseconds / 1000) % 60;
	int minutes = (int)((milliseconds / (1000 * 60)) % 60);
	int hours = (int)((milliseconds / (1000 * 60 * 60)) % 24);

	std::cout << "(" << hours << "h " << minutes << "m " << seconds << "s " << ms << "ms)" << std::endl;
	return "";
}
