#include "XORShift.h"

#include <iostream>


XORShift::XORShift()
{
	XORShift(time(0) * 1000);
}

XORShift::XORShift(int seed)
{
	last = seed;
}

int XORShift::nextInt(int max)
{
	last ^= (last << 21);
	last ^= (last >> 35);
	last ^= (last << 4);

	int64_t out = last % max;

	int done = (int)out;

	return (done < 0) ? -done : done;
}
