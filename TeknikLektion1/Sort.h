#pragma once
#include <vector>
#include <iostream>

class Sort
{
public:
	static void bubbleSort(std::vector<int>& a, int& numOperations);
	static void insertionSort(std::vector<int>& a, int& numOperations);
	static void cycleSort(std::vector<int>& a, int& numOperations);
	static void mergeSort(std::vector<int>& a, int& numOperations);
private:
	static void swap(int& a, int& b);
	static std::vector<int> merge(std::vector<int> a, std::vector<int> b, int& numOperations);
	static std::vector<int> startMergeSort(std::vector<int> a, int& numOperations);
};

