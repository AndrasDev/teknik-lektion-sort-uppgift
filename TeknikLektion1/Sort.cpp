#include "Sort.h"
#include <iostream>

void Sort::bubbleSort(std::vector<int>& a, int& numOperations)
{
	bool swapp = true;

	while (swapp) 
	{
		swapp = false;
		for (int i = 0; i < a.size() - 1; i++) 
		{
			if (a[i]>a[i + 1]) 
			{
				swap(a[i], a[i + 1]);
				swapp = true;
			}
		}
	}
}

void Sort::insertionSort(std::vector<int>& a, int& numOperations)
{
	int j = 0;

	for (int i = 1; i < a.size(); i++)
	{
		j = i;
		while (j > 0 && a[j - 1] > a[j])
		{
			swap(a[j], a[j - 1]);
			numOperations++;
			j--;
		}
	}
}

void Sort::cycleSort(std::vector<int>& a, int& numOperations)
{
	int writes = 0;

	for (int cycle_start = 0; cycle_start <= a.size() - 2; cycle_start++)
	{
		int item = a[cycle_start];

		int pos = cycle_start;
		for (int i = cycle_start + 1; i < a.size(); i++)
			if (a[i] < item)
				pos++;

		if (pos == cycle_start)
			continue;

		while (item == a[pos])
			pos += 1;

		if (pos != cycle_start)
		{
			swap(item, a[pos]);
			numOperations++;
			writes++;
		}

		while (pos != cycle_start)
		{
			pos = cycle_start;

			for (int i = cycle_start + 1; i < a.size(); i++)
				if (a[i] < item)
					pos += 1;

			while (item == a[pos])
				pos += 1;

			if (item != a[pos])
			{
				swap(item, a[pos]);
				numOperations++;
				writes++;
			}
		}
	}
}

void Sort::mergeSort(std::vector<int>& a, int& numOperations)
{
	a = startMergeSort(a, numOperations);
}

std::vector<int> Sort::startMergeSort(std::vector<int> a, int& numOperations)
{
	if (a.size() == 1)
		return a;

	std::vector<int> l1 = std::vector<int>();
	std::vector<int> l2 = std::vector<int>();

	for (int i = 0; i < a.size() / 2; i++)
		l1.push_back(a[i]);

	for (int i = a.size() - 1; i >= (a.size() / 2); i--)
		l2.push_back(a[i]);


	l1 = startMergeSort(l1, numOperations);
	l2 = startMergeSort(l2, numOperations);

	return merge(l1, l2, numOperations);
}


void Sort::swap(int & a, int & b)
{
	int temp = a;
	a = b;
	b = temp;
}

std::vector<int> Sort::merge(std::vector<int> a, std::vector<int> b, int& numOperations)
{
	std::vector<int> c = std::vector<int>();

	while (a.size() != 0 && b.size() != 0)
	{
		if (a[0] > b[0])
		{
			c.push_back(b[0]);
			b.erase(b.begin());
			numOperations++;
		}
		else
		{
			c.push_back(a[0]);
			a.erase(a.begin());
			numOperations++;
		}
	}
	while (a.size() != 0)
	{
		c.push_back(a[0]);
		a.erase(a.begin());
		numOperations++;
	}
	while (b.size() != 0)
	{
		c.push_back(b[0]);
		b.erase(b.begin());
		numOperations++;
	}

	return c;
}
