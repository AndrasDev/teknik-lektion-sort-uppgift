//XORshift is a Pseudorandom number generator implemented by Marcus Nilsson

#pragma once

#include <cstdint>
#include <ctime>

class XORShift
{
public:
	XORShift();
	XORShift(int seed);

	int nextInt(int max);

private:
	int64_t last;
	
};

